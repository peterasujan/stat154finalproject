#!/usr/bin/python

import os, csv, re

# os.chdir("C:\\Users\\kaiyinpoon\\Documents\\stat154finalproject")

with open("column_numbers.csv", "r") as file:
    numbers = file.readline()
    numbers_split = numbers.split()
    numbers_split = list(map(lambda n: int(n) - 1, numbers_split))

with open("word_feature.csv", "r") as file:
    names = file.readline()
    names_split = re.sub(r"[\",]", " ", names).split()
    names = [names_split[i] for i in numbers_split]

with open("column_names.csv", "w") as file:
    writer = csv.writer(file)
    for name in names:
        writer.writerows([[name]])
