#!/usr/bin/python

import os, csv, re, fileinput

file = open("column_names.csv", "r")
line = file.readline()
file.close()

words = line.split(",")
pattern = "[^0-9a-zA-Z'-]+"

csvfile = open("test_mat.csv", "w")
writer = csv.writer(csvfile)
writer.writerows([words])
for line in fileinput.input():
    line_split = re.sub(pattern, " ", line).lower().split()
    row = [0] * len(words)
    union = set(line_split) & set(words)
    indices = list(map(words.index, union))
    for i in indices:
        row[i] = row[i] + 1
    if sum(row) != 0:
        row = list(map(lambda count: count / sum(row), row))
    writer.writerows([row])
csvfile.close()
