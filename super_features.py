import re, csv, string
re.UNICODE = True

def percent_uppercase(s):
    """ Calculates the percent of letters that are capital. """
    upper_count = len(re.findall(r'[A-Z]', s))
    lower_count = len(re.findall(r'[a-z]', s))
    if upper_count + lower_count > 0:
        return float(upper_count) / (upper_count + lower_count)
    else:
        return 0

def percent_capitalized_words(s):
    num_words = len(re.findall(r'[A-Za-z][A-Za-z]*', s))
    num_uppercase = len(re.findall(r'[A-Z][A-Za-z]*', s))
    return (float(num_uppercase) / num_words) if num_words > 0 else 0

def internal_capitals(s):
    """ Number of words capitalized in the middle of sentences. """
    sentences = s.split(".")
    count = 0
    for sent in sentences:
        words = sent.split()[1:]
        count += len([w for w in words if re.match(r'[A-Z]', w)])

    return count

def length_message(s):
    """ Including spaces. """
    return len(s)

def percent_digits(s):
    """ Returns the percentage of numeric digits in the message. """
    digit_count = len(re.findall(r'[0-9]', s))
    return float(digit_count) / len(s)

def max_consecutive_digits(s):
    """ Calculates the longest number present in s. """
    numbers = re.findall(r'[.,0-9]+', s)
    return max([len(num) for num in numbers]) if numbers else 0

def count_character(s, c):
    """ Counts instances of c in s. """
    return len(re.findall(c, s))

def count_questions(s):
    """counts all question marks using count_character function"""
    return count_character(s,r'[\?]')

def count_periods(s):
    """counts all period marks using count_character function"""
    return count_character(s,r"[\.]")

def count_colons(s):
    """return counts of all colons"""
    return count_character(s,r'[\:]')

def count_commas(s):
    """return all counts of all commas"""
    return count_character(s,r'[\,]')

def percentage_punctuation(s):
    """returns a percentage of commas, colons, exclamations, periods, and question marks"""
    s= re.sub(r"[ ]", "", s)
    length=len(s)
    sum= count_exclamations(s)+count_commas(s)+count_colons(s)+count_questions(s)+count_periods(s)
    if sum>0:
        return float(sum) / length
    else:
        return 0


def count_money_signs(s):
    return count_character(s, ur'[\u00A3\$]')

def count_exclamations(s):
    return count_character(s, r'!')

def uppercase_free(s):
    """ Counts instances of 'FREE.' """
    return len(re.findall(r'FREE', s))

def count_text_and_phone_addresses(s):
    """ Counts the instances of 5 or 6-digit text message addresses. """
    return len([num for num in re.findall(r'[0-9]+', s) if len(num) in (5, 6, 10)])

def count_co_addresses(s):
    """ Counts the occurrences of ".co" (i.e. a website) in the message.
    I may add more sophisticated url checking later. """
    return len(re.findall(r'\.co', s))



# add any functions to this dictionary to add them as a super feature
# the key is the column header and the value is the name of the function
functions = {'percent_upper': percent_uppercase,
             'percent_cap_words': percent_capitalized_words,
             'internal_caps': internal_capitals,
             'length': length_message,
             'percent_digits': percent_digits,
             'longest_number': max_consecutive_digits,
             'money_signs': count_money_signs,
             'FREE': uppercase_free,
             'text_and_phone_addresses': count_text_and_phone_addresses,
             'co_addresses': count_co_addresses,
             "count_commas": count_commas,
             "count_colons": count_colons,
             "count_exclamations": count_exclamations,
             "count_questions": count_questions,
             "count_periods": count_periods,
             "percentage_punctuation": percentage_punctuation}

def calculate_super_features(s):
    """ Calculates all super features for s. """
    super_features = []
    for f in functions.values():
        super_features.append(f(s))

    return super_features

txtfile=raw_input("Enter input text file 'example.txt': ") ###added file input
csvfile_output=raw_input("Name output Super Features csv 'example.csv':") ###added naming file output


if __name__ == '__main__':
    print "Number of super features: {0}".format(len(functions))
    csvfile = open(csvfile_output, "w")
    writer = csv.writer(csvfile)
    writer.writerows([["isHam"] + ["super_" + s for s in functions.keys()]])
    with open(txtfile, "r") as file:  
        for line in file:
            line_split = line.split()
            isHam = (line_split[0] == "ham")
            if isHam == "ham":
                rest = line[4:]
            else:
                rest = line[5:]
            entry = [isHam] + calculate_super_features(rest)
            
            writer.writerows([entry])
    csvfile.close()
