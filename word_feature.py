#!/usr/bin/python

import os, csv, re, sys

common_file = open("common-english-words.txt", "r")
common = common_file.readline().split(",")
common_file.close()

pattern = "[^0-9a-zA-Z'-]+"

unique = set()

txtfile=raw_input("Enter input 'example.txt': ")  #enable entering whichever txt file to collect data on
csvfile_output=raw_input("Name Output Word Feature frequency csv 'example.csv': ")  ###enabling output naming
csvfile_2_output=raw_input("Name Output Word Feature csv 'example.csv': ")  ###enabling output naming
csvfile_3_output=raw_input("Name Output Word Feature count csv 'example.csv': ")

csvfile = open("label_text.csv", "w")
#with open("train_msgs.txt", "r") as file:  original
with open(txtfile, "r") as file:
    writer = csv.writer(csvfile)
    for line in file:
        line_split = re.sub(pattern, " ", line).lower().split()
        label = line_split[0]
        if label == "ham":
            rest = line[4:]
        else:
            rest = line[5:]
        entry = [label, rest]
        writer.writerows([entry])
        for word in line_split[1:]:
            if word not in common:
                unique.add(word)
csvfile.close()

unique = list(unique)
csvfile = open(csvfile_output, "w")
csvfile_2 = open(csvfile_2_output, "w")
csvfile_3 = open(csvfile_3_output, "w")

#with open("train_msgs.txt", "r") as file:   orginal
with open(txtfile, "r") as file:
    header = ["isHam"] + unique
    writer = csv.writer(csvfile)
    writer_2 = csv.writer(csvfile_2)
    writer_3 = csv.writer(csvfile_3)
    writer.writerows([header])
    writer_2.writerows([header])
    writer_3.writerows([header])
    for line in file:
        row = [0] * (len(unique) + 1)
        row_2 = [0] * (len(unique) + 1)
        line = re.sub(pattern, " ", line).lower().split()
        union = set(line[1:]) & set(unique)
        indices = list(map(unique.index, union))
        for i in indices:
            row[i + 1] = row[i + 1] + 1
            row_2[i + 1] = 1
        row[0] = int(line[0] == "ham")
        writer_3.writerows([row])
        if len(line[1:]) != 0:
            row = list(map(lambda count: count / len(line[1:]), row))
        row[0] = int(line[0] == "ham")
        row_2[0] = int(line[0] == "ham")
        writer.writerows([row])
        writer_2.writerows([row_2])
csvfile.close()
csvfile_2.close()
csvfile_3.close()
