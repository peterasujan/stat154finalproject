\contentsline {section}{\numberline {1}Results}{1}
\contentsline {subsection}{\numberline {1.1}Introduction}{1}
\contentsline {subsection}{\numberline {1.2}Answers to Questions}{1}
\contentsline {subsubsection}{\numberline {1.2.1}Data Description}{1}
\contentsline {subsubsection}{\numberline {1.2.2}Words Feature Creation}{2}
\contentsline {subsubsection}{\numberline {1.2.3}Unsupervised Feature Filtering}{2}
\contentsline {subsubsection}{\numberline {1.2.4}Super Features Extraction}{2}
\contentsline {subsubsection}{\numberline {1.2.5}Combined Features}{3}
\contentsline {subsubsection}{\numberline {1.2.6}Classification on Filtered Word Features}{3}
\contentsline {subsubsection}{\numberline {1.2.7}Validation on Filtered Word Feature Classifier}{4}
\contentsline {subsubsection}{\numberline {1.2.8}Classification on Super Features}{5}
\contentsline {subsubsection}{\numberline {1.2.9}Classification on Filtered Word and Super Features}{7}
\contentsline {subsubsection}{\numberline {1.2.10}Validation on Filtered Word and Super Feature Classifier}{7}
\contentsline {paragraph}{Method}{9}
\contentsline {subsection}{\numberline {1.3}Summary and Conclusions}{9}
\contentsline {section}{\numberline {2}Appendix: R Code}{10}
